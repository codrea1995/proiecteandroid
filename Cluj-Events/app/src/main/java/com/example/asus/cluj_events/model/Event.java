package com.example.asus.cluj_events.model;

import java.io.Serializable;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by Denisa on 08.11.2016.
 */

public class Event implements Serializable {
    private int id;
    private String title;
    private String place;
    private String date;


    public Event(){}

    public Event( String title, String place, String date){
        this.id = -1;
        this.title = title;
        this.place = place;
        this.date = date;
    }
    public Event(int id, String title, String place, String date){
        this.id = id;
        this.title = title;
        this.place = place;
        this.date = date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlace() { return place; }

    public void setPlace(String place) {this.place = place;}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String toString(){
        String str = this.id + " " + this.title + " " + this.place + " " + this.date + " ";
        return str;
    }


}

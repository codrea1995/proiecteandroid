package com.example.asus.cluj_events;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActiviy extends AppCompatActivity {
    Button bRegister;
    EditText etEmail, etPassword;
    FirebaseAuth firebase;

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_activiy);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bRegister = (Button) findViewById(R.id.bRegister);
        progressDialog = new ProgressDialog(this);
        firebase = FirebaseAuth.getInstance();

        if (firebase.getCurrentUser() != null){
            Intent intent = new Intent(RegisterActiviy.this, MainActivity.class);
            startActivity(intent);
        }


        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etEmail.getText().toString();
                String pass = etPassword.getText().toString();
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)){
                    Toast.makeText(RegisterActiviy.this, "Please complete all fields!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog.setMessage("Registering user...");
                progressDialog.show();

                firebase.createUserWithEmailAndPassword(email,pass)
                        .addOnCompleteListener(RegisterActiviy.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            Toast.makeText(RegisterActiviy.this,"Register Succefully",Toast.LENGTH_LONG).show();
                            if (firebase.getCurrentUser() != null){
                                Intent intent = new Intent(RegisterActiviy.this, MainActivity.class);
                                startActivity(intent);
                            }
                        }else{
                            Toast.makeText(RegisterActiviy.this,"Couldn't register, try again",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}

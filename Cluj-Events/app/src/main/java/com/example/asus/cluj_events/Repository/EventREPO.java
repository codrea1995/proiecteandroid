package com.example.asus.cluj_events.Repository;

import android.app.Application;

import com.example.asus.cluj_events.model.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bogdan on 1/11/2017.
 */

public class EventREPO extends Application {
    private List<Event> eventList=new ArrayList<>();
    public EventREPO (){
        Event event = new Event("Food1", "Delicious", "Do something nice");
        eventList.add(event);

        event = new Event("aaaa", "aaaaa", "aaaa");
        eventList.add(event);

        event = new Event("bbbbb", "bbbbb", "bbbbb");
        eventList.add(event);


        event = new Event("ccccc", "cccccc", "cccccc");
        eventList.add(event);


    }
    public List<Event> getList(){
        return eventList;
    }
    public boolean update(int pos,Event event){
        if(pos<0 || pos>eventList.size()){
            return false;
        }else{
            eventList.add(pos,event);
            eventList.remove(pos+1);

            return true;
        }

    }
    public void add(Event event){
        eventList.add(event);
    }
}

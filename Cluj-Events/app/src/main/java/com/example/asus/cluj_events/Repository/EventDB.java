package com.example.asus.cluj_events.Repository;

/**
 * Created by Bogdan on 1/11/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.asus.cluj_events.model.Event;

import java.util.ArrayList;
import java.util.List;


public class EventDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Event.db";
    public static final String TABLE_NAME = "event_table";
    public static final String ID = "ID";
    public static final String TITLE = "TITLE";
    public static final String PLACE = "PLACE";
    public static final String DATE = "DATE";

    public EventDB(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+" (ID INTEGER PRIMARY KEY AUTOINCREMENT ,TITLE TEXT,PLACE TEXT,DATE TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
    public boolean insertData(Event event){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(TITLE,event.getTitle());
        contentValues.put(PLACE, event.getPlace());
        contentValues.put(DATE, String.valueOf(event.getDate()));
        long res=db.insert(TABLE_NAME,null,contentValues);

        Log.d("inBazaTitlu",event.getTitle());
        Log.d("inBazaLoc",event.getPlace());
        Log.d("inBazaData",String.valueOf(event.getDate()));
        Log.d("inBazaID",String.valueOf(event.getId()));
        Log.d("inBazaRes",String.valueOf(res));

        if(res==-1)
            return false;
        return true;
    }
    public List<Event> getAllData(){
        List<Event> data=new ArrayList<>();
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res= db.rawQuery("select * from "+TABLE_NAME,null);
        while(res.moveToNext()){
            Event event=new Event(Integer.valueOf(res.getString(0)),res.getString(1),res.getString(2),res.getString(3));
            data.add(event);
        }
        return data;
    }

    public boolean deleteEvent(int id){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(ID,id);
        Log.d("idul",String.valueOf(id));
        return db.delete(TABLE_NAME, "id=" + id, null) > 0;

    }

    public boolean updateData(Event event){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(ID,event.getId());
        contentValues.put(TITLE,event.getTitle());
        contentValues.put(PLACE, event.getPlace());
        contentValues.put(DATE, String.valueOf(event.getDate()));
        int res=db.update(TABLE_NAME,contentValues,"id = ?" ,new String[] {String.valueOf(event.getId())} );
        if(res>0)
            return true;
        return false;
    }
}

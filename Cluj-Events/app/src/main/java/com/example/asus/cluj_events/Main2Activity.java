package com.example.asus.cluj_events;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.cluj_events.Repository.EventDB;
import com.example.asus.cluj_events.Repository.EventREPO;
import com.example.asus.cluj_events.model.Event;
import com.example.asus.cluj_events.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //ArrayList<Event> events = new ArrayList<>();
    List<Event> events = new ArrayList<>();
    EventREPO repo;
    //List<Event> eventsList = new ArrayList<>();
    ArrayAdapter<Event> adapter;
    RecyclerView recyclerView;
    EventREPO eventREPO;
    EventDB db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new EventDB(this);
        events = db.getAllData();
        Log.d("verificare",events.toString());
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.faab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Main2Activity.this, AddEventActivity.class);
                startActivity(intent);
            }
        });
        //populate();


        repo=(EventREPO) this.getApplication();
        events = db.getAllData();
        Log.d("utilizatorii",events.toString());
        adapter = new ArrayAdapter<Event>(this, R.layout.list_layout, events);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);






        registerClickListener();
        NavigationView navigationView2 = (NavigationView) findViewById(R.id.nav_view);
        navigationView2.setNavigationItemSelectedListener(this);
        View header = navigationView2.getHeaderView(0);


        TextView userEmail = (TextView)header.findViewById(R.id.navBarUserEmail);
        TextView userName = (TextView)header.findViewById(R.id.navBArUserName);

        Intent intent = this.getIntent();
        final User user = (User) intent.getSerializableExtra("user");
//        Log.d("userName1", user.getUserName());
//        userName.setText(user.getUserName());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void populate() {
        repo=(EventREPO) this.getApplication();
        events = db.getAllData();
        Log.d("utilizatorii",events.toString());
        adapter = new ArrayAdapter<Event>(this, R.layout.list_layout, events);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
//        for (int i = 1; i <= 4; i++) {
//            Event event = new Event();
//            event.setId(i);
//            event.setTitle("Event nr " + i);
//            event.setPlace("Place nr " + i);
//            Date date = new Date();
//            event.setDate(date.toString());
//            events.add(event);
//
//            adapter = new ArrayAdapter<Event>(this, R.layout.list_layout, events);
//            ListView listView = (ListView) findViewById(R.id.listView);
//            listView.setAdapter(adapter);
//        }
    }


    private void registerClickListener() {
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                String message = "Show details for " + textView.getText();
                Toast.makeText(Main2Activity.this, message, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Main2Activity.this, EventDetailsActivity.class);
                intent.putExtra("event", events.get(position));
                startActivityForResult(intent, 1);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 2){
            Event newBook = (Event) data.getSerializableExtra("updated");
            for (Event b : events) {
                if(newBook.getId() == b.getId()){
                    b.setPlace(newBook.getPlace());
                    b.setTitle(newBook.getTitle());
                    b.setDate(newBook.getDate());
                }
            }
            adapter.notifyDataSetChanged();
        }
        else if (resultCode == 4){
            repo=(EventREPO) this.getApplication();
            events = db.getAllData();
            Log.d("verificare2",events.toString());
            Log.d("utilizatorii2",events.toString());
            adapter = new ArrayAdapter<Event>(this, R.layout.list_layout, events);
            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            startActivity(new Intent(Main2Activity.this, LogInActivity.class));
        }
        else if (id == R.id.nav_editUser) {
            startActivity(new Intent(Main2Activity.this, UserDetailsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

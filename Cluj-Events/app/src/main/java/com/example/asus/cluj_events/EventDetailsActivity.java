package com.example.asus.cluj_events;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.asus.cluj_events.Repository.EventDB;
import com.example.asus.cluj_events.Repository.EventREPO;
import com.example.asus.cluj_events.model.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.List;

public class EventDetailsActivity extends AppCompatActivity {

    private EventREPO repo;
    private int id;
    private String title,place,date,pos;
    private EditText titleText,placeText,dateText;
    private EventDB db;
    List<Event> events;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase = null;
    DatabaseReference databaseReference = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        populate();
        db=new EventDB(this);
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        repo=(EventREPO) this.getApplication();

        Button btnEmail = (Button) findViewById(R.id.btnEmail);

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailBtn = new Intent(Intent.ACTION_SEND);
                emailBtn.putExtra(Intent.EXTRA_EMAIL, new String[]{"codrea@gmail.com"});
                emailBtn.putExtra(Intent.EXTRA_CC, new String[]{"bogdan@gmail.com"});
                emailBtn.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailBtn.putExtra(Intent.EXTRA_TEXT, "body");

                emailBtn.setType("message/rfc822");
                startActivity(Intent.createChooser(emailBtn, "Chose email client..."));
            }
        });





    }

    public void populate(){
        Intent intent = getIntent();

        id= Integer.parseInt(intent.getStringExtra("id"));
        title = intent.getStringExtra("title");
        place = intent.getStringExtra("place");
        date = intent.getStringExtra("date");
        pos=intent.getStringExtra("pos");


        titleText = (EditText) findViewById(R.id.editTextTitle1);
        titleText.setText(title);

        placeText = (EditText) findViewById(R.id.editTextPlace1);
        placeText.setText(place);

        dateText = (EditText) findViewById(R.id.editTextDate1);
        dateText.setText(date);

    }

    public void pushUpdate(View view){
        Event event=new Event(id,titleText.getText().toString(),placeText.getText().toString(),dateText.getText().toString());
        Log.d("evenimentulUpdate",event.toString());
        if(!db.updateData(event))
            Snackbar.make(view, "Error Updating", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Log.d("eroarea1",event.toString());
        if(!repo.update(Integer.parseInt(pos), event)){
            Snackbar.make(view, "Error Updating", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Log.d("eroarea2",event.toString());
        }
        FirebaseUser user = firebaseAuth.getCurrentUser();
        events = db.getAllData();
        databaseReference.child(user.getUid()).setValue(events);
        Intent intent=new Intent(EventDetailsActivity.this,MainActivity.class);
        startActivity(intent);
    }

    public void pushDelete(View view) {
        db.deleteEvent(id);
        FirebaseUser user = firebaseAuth.getCurrentUser();
        events = db.getAllData();
        databaseReference.child(user.getUid()).setValue(events);
        Intent intent=new Intent(EventDetailsActivity.this,MainActivity.class);
        startActivity(intent);
    }


}

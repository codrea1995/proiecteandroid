package com.example.asus.cluj_events;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asus.cluj_events.Repository.EventDB;
import com.example.asus.cluj_events.Repository.EventREPO;
import com.example.asus.cluj_events.model.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddEventActivity extends AppCompatActivity {

    private FirebaseUser user;
    private EditText nameTitle, typePlace;
    private EventREPO repo;
    private EventDB db;
    Button createButton;
    List <Event> events;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase = null;
    DatabaseReference databaseReference = null;
    static final int DIALOG_ID = 0;
    private int year,month,day;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        firebaseAuth = FirebaseAuth.getInstance();
        db = new EventDB(this);
        nameTitle = (EditText) findViewById(R.id.createTitle);
        typePlace = (EditText) findViewById(R.id.createPlace);
        createButton = (Button) findViewById(R.id.updateButton13);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        final Calendar cal=Calendar.getInstance();
        year=cal.get(Calendar.YEAR);
        month=cal.get(Calendar.MONTH);
        day=cal.get(Calendar.DAY_OF_MONTH);


//        createButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(AddEventActivity.this, Main2Activity.class);
//                startActivity(intent);
//
//            }
//        });
    }


    public void pushCreate(View view) {
        repo = (EventREPO) this.getApplication();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        Event event = new Event(nameTitle.getText().toString(), typePlace.getText().toString(),new Date().toString());
        repo.add(event);
        db.insertData(event);
        events = db.getAllData();
        databaseReference.child(user.getUid()).setValue(events);
        Toast.makeText(AddEventActivity.this, "Information saved...", Toast.LENGTH_LONG);
        Intent intent = new Intent(AddEventActivity.this, MainActivity.class);
        startActivity(intent);

    }
    public void showDialog(View view) {
        showDialog(DIALOG_ID);
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if(id==DIALOG_ID)
            return new DatePickerDialog(AddEventActivity.this,kTimePickListener,year,month,day);
        return null;
    }
    protected DatePickerDialog.OnDateSetListener kTimePickListener=
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year1, int monthOfYear, int dayOfMonth) {
                    year=year1;
                    month=monthOfYear+1;
                    day=dayOfMonth;
                    Toast.makeText(AddEventActivity.this,year+"/"+month+"/"+day, Toast.LENGTH_LONG).show();
                }
            };
}

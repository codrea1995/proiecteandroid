package com.example.asus.cluj_events.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Bogdan on 12/10/2016.
 */

public class User implements Serializable {
    private String email;
    private String password;


    public User(String email, String password){
        this.email = email;
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }



    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }





}

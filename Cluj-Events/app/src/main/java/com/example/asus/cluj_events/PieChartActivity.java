package com.example.asus.cluj_events;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class PieChartActivity extends AppCompatActivity {

    float[] yData = {23.3f,10.6f,66.67f,44.32f,46.01f,16.89f,23.9f};
    String[] xData = {"a","b","c","d","e","f","g"};
    PieChart pieChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_chart);
        pieChart = (PieChart) findViewById(R.id.pieChart);
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("super chart");
        pieChart.setCenterTextSize(10);
        pieChart.setDrawEntryLabels(true);

        addDataSet(pieChart);
    }

    private void addDataSet(PieChart pieChart) {
        ArrayList <PieEntry> yEntry = new ArrayList<>();
        ArrayList <String> xEntry = new ArrayList<>();

        for (int i = 0; i < yData.length;i++){
            yEntry.add(new PieEntry(yData[i], i));

        }
        for (int i = 0; i < xData.length; i++){
            xEntry.add(xData[i]);

        }

        PieDataSet pieDataSet = new PieDataSet(yEntry, "Employee Sale");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);

        ArrayList <Integer> colors = new ArrayList<>();
        colors.add(Color.GRAY);
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.CYAN);
        colors.add(Color.YELLOW);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);
        

    }
}

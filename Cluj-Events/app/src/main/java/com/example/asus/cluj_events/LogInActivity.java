package com.example.asus.cluj_events;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.cluj_events.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity {

    Button bLogin;
    EditText etUserEmail, etPassword;
    TextView registerLink;
    private FirebaseAuth firebaseAuth;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null){
            Intent intent = new Intent(LogInActivity.this, MainActivity.class);
            startActivity(intent);
        }
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        registerLink = (TextView) findViewById(R.id.tvRegisterLink);
        progressDialog = new ProgressDialog(this);
        bLogin = (Button) findViewById(R.id.bLogin);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etUserEmail.getText().toString();
                String pass = etPassword.getText().toString();

                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)){
                    Toast.makeText(LogInActivity.this, "Please complete all fields!", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressDialog.setMessage("Login, please wait...");
                progressDialog.show();

                firebaseAuth.signInWithEmailAndPassword(email,pass)
                        .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressDialog.dismiss();
                                if (task.isSuccessful()){
                                    Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(LogInActivity.this,"Couldn't log in, try again",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
//                User user = new User();
//                user.setUserName(etUserName.getText().toString());
//
//                user.setPassword(etPassword.getText().toString());
//                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
//                Log.d("userName", etUserName.getText().toString());
//                intent.putExtra("user", user);
//                startActivityForResult(intent, 1);
////                startActivity(new Intent(LogInActivity.this, Main2Activity.class));
            }
        });

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogInActivity.this, RegisterActiviy.class));
            }
        });
    }
}
